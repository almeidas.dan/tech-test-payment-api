﻿using Payment.Domain.Interfaces.Repositories;
using Payment.Domain.Model;
using Payment.Infra.Data;

namespace Payment.Infra.Repository
{
    public class RepositoryVenda : IRepositoryVenda
    {
        private readonly PaymentContext _context;
        public RepositoryVenda(PaymentContext context)
        {
            _context = context;
        }

        public async Task AddAsync(Venda venda)
        {
            var vendedor = await _context.Vendedores.FindAsync(venda.Vendedor.Id);
            if (vendedor != null) venda.Vendedor = vendedor;

            await _context.Vendas.AddAsync(venda);
            await _context.SaveChangesAsync();
        }

        public async Task<Venda> GetByIdAsync(int id)
        {
            var venda = await _context.Vendas.FindAsync(id);
            if (venda == null) throw new ArgumentException("Nenhum dado encontrado com esse id");

            _context.Entry(venda).Reference(v => v.Vendedor).Load();
            _context.Entry(venda).Collection(v => v.Itens).Load();

            return venda;
        }

        public async Task UpdateAsync(Venda venda)
        {
            _context.Vendas.Update(venda);
            await _context.SaveChangesAsync();
        }
    }
}
