using System.ComponentModel.DataAnnotations;

namespace Payment.Domain.Model
{
    public class Item: Base
    {
        [Required]
        [StringLength(100)]
        public string Descricao { get; set; }

        [Required]
        public int Quantidade { get; set; }
    }
}