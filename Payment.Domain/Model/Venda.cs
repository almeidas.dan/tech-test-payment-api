using Payment.Domain.Model.Enum;
using Payment.Domain.Services;

namespace Payment.Domain.Model;

public sealed class Venda:Base
{
    private Venda(DateTime data, EnumStatusVenda status)
    {
        this.Data = data;
        this.Status = status;
    }
    
    public DateTime Data { get; set; }
    public EnumStatusVenda Status { get; set; }

    public int VendedorId { get; set; }
    public Vendedor Vendedor { get; set; }

    public ICollection<Item> Itens { get; set; }

    public static Venda Create(DateTime data)
    {
        return new Venda(data, EnumStatusVenda.AguardandoPagamento);
    }

    public static bool UpateStatus(EnumStatusVenda statusAtual, EnumStatusVenda statusNovo)
    {
        return StatusVendasPermitidos.StatusVendaPermitido(statusAtual, statusNovo);
    }
}
