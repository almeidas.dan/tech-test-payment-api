using System.ComponentModel.DataAnnotations;

namespace Payment.Domain.Model;

public class Vendedor : Base
{
    
    [Required]
    public string Nome { get; set; }
    [Required]
    public string Cpf { get; set; }
    [Required]
    public string Email  { get; set; } 
    [Required]   
    public string Telefone { get; set; }
}
