namespace Payment.Domain.Model.Enum;

public enum EnumStatusVenda
{
    AguardandoPagamento,
    PagamentoAprovado,
    EnviadoTransportadora,
    Entregue,
    Cancelado
}
