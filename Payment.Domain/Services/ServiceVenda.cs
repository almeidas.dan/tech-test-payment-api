﻿using Payment.Domain.Interfaces.Repositories;
using Payment.Domain.Interfaces.Services;
using Payment.Domain.Model;

namespace Payment.Domain.Services
{
    public class ServiceVenda : IServiceVenda
    {
        public readonly IRepositoryVenda _repositoryVenda;

        public ServiceVenda(IRepositoryVenda repositoryVenda)
        {
            _repositoryVenda = repositoryVenda;
        }
        public async Task AddAsync(Venda venda)
        {
            await _repositoryVenda.AddAsync(venda);
        }

        public async Task<Venda> GetByIdAsync(int id)
        {
            var venda = await _repositoryVenda.GetByIdAsync(id);
            return venda;
        }

        public async Task UpdateAsync(Venda venda)
        {
            await _repositoryVenda.UpdateAsync(venda);
        }
    }
}
