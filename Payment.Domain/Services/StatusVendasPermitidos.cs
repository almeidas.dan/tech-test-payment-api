﻿using Payment.Domain.Model.Enum;

namespace Payment.Domain.Services
{
    public static class StatusVendasPermitidos
    {
        private readonly static Dictionary<EnumStatusVenda, List<EnumStatusVenda>> _statusPermitidos =
                new Dictionary<EnumStatusVenda, List<EnumStatusVenda>>
                {
                    { EnumStatusVenda.AguardandoPagamento,
                        new List<EnumStatusVenda> { EnumStatusVenda.PagamentoAprovado, EnumStatusVenda.Cancelado }},
                    { EnumStatusVenda.PagamentoAprovado,
                        new List<EnumStatusVenda> { EnumStatusVenda.EnviadoTransportadora, EnumStatusVenda.Cancelado }},
                    { EnumStatusVenda.EnviadoTransportadora,
                        new List<EnumStatusVenda> { EnumStatusVenda.Entregue}}
                };

        public static bool StatusVendaPermitido(EnumStatusVenda atual, EnumStatusVenda novo)
        {
            return _statusPermitidos.ContainsKey(atual) && _statusPermitidos[atual].Contains(novo);
        }
    }
}
