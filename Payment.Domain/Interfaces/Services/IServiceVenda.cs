﻿using Payment.Domain.Model;

namespace Payment.Domain.Interfaces.Services
{
    public interface IServiceVenda
    {
        Task AddAsync(Venda venda);
        Task<Venda> GetByIdAsync(int id);
        Task UpdateAsync(Venda venda);
    }
}
