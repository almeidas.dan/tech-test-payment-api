﻿using Payment.Domain.Model;

namespace Payment.Domain.Interfaces.Repositories
{
    public interface IRepositoryVenda 
    {
        Task AddAsync(Venda venda);
        Task<Venda> GetByIdAsync(int id);
        Task UpdateAsync(Venda venda);
    }
}
