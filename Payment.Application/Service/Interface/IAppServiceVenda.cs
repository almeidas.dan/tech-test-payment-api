﻿using Payment.Application.Service.Model.Request;
using Payment.Application.Service.Model.Response;

namespace Payment.Application.Service.Interface
{
    public interface IAppServiceVenda
    {
        Task<CreateVendaResponse> CreateAsync(CreateVendaRequest request);
        Task<GetByIdVendaResponse> GetByIdAsync(int id);
        Task<UpdateVendaResponse> UpdateAsync(int id, UpdateVendaRequest status);
    }
}
