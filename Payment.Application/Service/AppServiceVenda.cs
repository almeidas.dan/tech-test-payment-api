﻿using Microsoft.Extensions.Logging;
using Payment.Application.Common;
using Payment.Application.Service.Interface;
using Payment.Application.Service.Model.Dtos;
using Payment.Application.Service.Model.Request;
using Payment.Application.Service.Model.Response;
using Payment.Domain.Interfaces.Services;
using Payment.Domain.Model;

namespace Payment.Application.Service
{
    public class AppServiceVenda : BaseService<AppServiceVenda> , IAppServiceVenda
    {
        public readonly IServiceVenda _service;

        public AppServiceVenda(ILogger<AppServiceVenda> logger, IServiceVenda service) : base(logger)
        {
            _service = service;
        }

        public async Task<CreateVendaResponse> CreateAsync(CreateVendaRequest request)
        {
            if (request == null)
                throw new ArgumentException("Request empty!");

            if (request.Itens.Count() < 1  || request.Itens.Any((item) => item.Quantidade < 1))
                throw new ArgumentException("A venda deve possuir pelo menos 1 item!");

            var novaVenda = Venda.Create(request.Data);

            if (novaVenda.Vendedor == null)
                novaVenda.Vendedor = new Vendedor()
                {
                    Id = request.Vendedor.Id == 0 ? 1 : request.Vendedor.Id,
                    Nome = request.Vendedor.Nome,
                    Cpf = request.Vendedor.Cpf,
                    Email = request.Vendedor.Email,
                    Telefone = request.Vendedor.Telefone
                };
            novaVenda.Itens = new List<Item>();
            foreach (var item in request.Itens)
            {
                var _item = new Item()
                {
                    Descricao = item.Descricao,
                    Quantidade = item.Quantidade
                };
                novaVenda.Itens.Add(_item);
            }

            await _service.AddAsync(novaVenda);

            return new CreateVendaResponse() { Id = novaVenda.Id };
        }

        public async Task<GetByIdVendaResponse> GetByIdAsync(int id)
        {
            var response = new GetByIdVendaResponse();

            var venda = await _service.GetByIdAsync(id);

            response.Venda = (VendaDTO)venda;

            return response;
        }

        public async Task<UpdateVendaResponse> UpdateAsync(int id, UpdateVendaRequest status)
        {
            var venda = await _service.GetByIdAsync(id);
            if (venda == null) throw new ArgumentNullException("Venda não encontrada");

            if(!Venda.UpateStatus(venda.Status, status.StatusVenda))
                throw new ArgumentNullException("Status não permitido");

            venda.Status = status.StatusVenda;
            await _service.UpdateAsync(venda);

            return new UpdateVendaResponse();
        }
    }
}
