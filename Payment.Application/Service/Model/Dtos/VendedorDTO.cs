﻿using System.ComponentModel.DataAnnotations;

namespace Payment.Application.Service.Model.Dtos
{
    public class VendedorDTO
    {
        public int Id { get; set; }
        [Required]
        public string Nome { get; set; }
        [Required]
        public string Cpf { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Telefone { get; set; }
    }
}
