﻿using Payment.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace Payment.Application.Service.Model.Dtos
{
    public class ItemDTO
    {
        public ItemDTO()
        { }
        public ItemDTO(Item item)
        {
            Descricao= item.Descricao;
            Quantidade= item.Quantidade;
        }
        
        [Required]
        [StringLength(100)]
        public string Descricao { get; set; }

        [Required]
        public int Quantidade { get; set; }
    }
}
