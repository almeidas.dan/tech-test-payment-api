﻿using Newtonsoft.Json.Converters;
using Payment.Domain.Model;
using Payment.Domain.Model.Enum;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Payment.Application.Service.Model.Dtos
{
    public class VendaDTO
    {
        public int Id { get; set; }
        public DateTime Data { get; set; }

        [Required]
        [EnumDataType(typeof(EnumStatusVenda))]
        [JsonConverter(typeof(StringEnumConverter))]
        public EnumStatusVenda Status { get; set;}
        public VendedorDTO Vendedor { get; set; }

        public ICollection<ItemDTO> Itens { get; set; }

        public static explicit operator VendaDTO(Venda v)
        {
            var venda = new VendaDTO()
            {
                Id = v.Id,
                Data = v.Data,
                Status = v.Status,
                Vendedor = new VendedorDTO()
                {
                    Id = v.VendedorId,
                    Nome = v.Vendedor.Nome,
                    Cpf = v.Vendedor.Cpf,
                    Email = v.Vendedor.Email,
                    Telefone = v.Vendedor.Telefone
                }
            };

            venda.Itens = new List<ItemDTO>();

            foreach (var item in v.Itens)
            {
                venda.Itens.Add(new ItemDTO(item));
            }

            return venda;
        }
    }
}
