﻿using Payment.Application.Common;
using Payment.Application.Service.Model.Dtos;

namespace Payment.Application.Service.Model.Response
{
    public class GetByIdVendaResponse : BaseResponse
    {
        public VendaDTO Venda { get; set; }
    }
}
