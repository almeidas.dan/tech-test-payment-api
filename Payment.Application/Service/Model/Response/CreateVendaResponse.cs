﻿using Payment.Application.Common;

namespace Payment.Application.Service.Model.Response
{
    public class CreateVendaResponse : BaseResponse
    {
        public int Id { get; set; }
    }
}
