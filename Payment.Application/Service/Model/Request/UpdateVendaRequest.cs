﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Payment.Domain.Model.Enum;
using System.ComponentModel.DataAnnotations;

namespace Payment.Application.Service.Model.Request
{
    public class UpdateVendaRequest
    {
        [Required]
        [EnumDataType(typeof(EnumStatusVenda))]
        [JsonConverter(typeof(StringEnumConverter))]
        public EnumStatusVenda StatusVenda { get; set; }
    }
}
