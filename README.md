## Sobre o Projeto
Projeto de API REST para a realização de desafio técnico.
- **.NET 7**


# Pottencial - Payment API
![](prints/Swagger.png)

## Aplicação
- Construida uma API REST utilizando .Net Core;
- A API possuir 3 operações:
  1) Registrar venda;
  2) Buscar venda: Busca pelo Id da venda;
  3) Atualizar venda.
  
- Uma venda contém informação sobre o vendedor que a efetivou, data, identificador do pedido e os itens que foram vendidos;
- O vendedor possui id, cpf, nome, e-mail e telefone;
- A inclusão de uma venda deve possuir pelo menos 1 item;
- A atualização de status permite somente as seguintes transições: 
  - De: `Aguardando pagamento` Para: `Pagamento Aprovado`
  - De: `Aguardando pagamento` Para: `Cancelada`
  - De: `Pagamento Aprovado` Para: `Enviado para Transportadora`
  - De: `Pagamento Aprovado` Para: `Cancelada`
  - De: `Enviado para Transportador`. Para: `Entregue`
- A API não possui mecanismos de autenticação/autorização;
- A aplicação implementar os mecanismos de persistência "em memória".

