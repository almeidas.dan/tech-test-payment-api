﻿using Microsoft.EntityFrameworkCore;
using Payment.Domain.Model;

namespace Payment.Infra.Data
{
    public class PaymentContext : DbContext
    {
        public PaymentContext()
        {
        }

        public PaymentContext(DbContextOptions<PaymentContext> options) : base(options) { }

        public DbSet<Item> Itens { get; set; }
        public DbSet<Vendedor> Vendedores { get; set; }
        public DbSet<Venda> Vendas { get; set; }
    }
}
