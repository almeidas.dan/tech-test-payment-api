﻿using Microsoft.AspNetCore.Mvc;
using Payment.Application.Service.Interface;
using Payment.Application.Service.Model.Request;

namespace Payment.Presentation.API.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class VendaController : BaseController
    {
        private readonly IAppServiceVenda _appServiceVenda;
        public VendaController(IAppServiceVenda appServiceVenda) 
        {
            _appServiceVenda = appServiceVenda;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateVendaRequest request)
        {
            try
            {
                var action = await _appServiceVenda.CreateAsync(request);
                return CreatedAtAction(actionName: nameof(Get),routeValues: new { id = action.Id },value: action);
            }
            catch (ArgumentException ex)
            {
                return ArgumentExceptionHandling(ex);
            }
            catch (Exception ex)
            {
                return ExceptionHandling(ex);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var action = await _appServiceVenda.GetByIdAsync(id);
                return Ok(action);
            }
            catch (ArgumentException ex)
            {
                return ArgumentExceptionHandling(ex);
            }
            catch (Exception ex)
            {
                return ExceptionHandling(ex);
            }
        }

        [HttpPatch("{id}")]
        public async Task<IActionResult> UpdateStatus(int id, [FromBody] UpdateVendaRequest request)
        {
            try
            {
                var action = await _appServiceVenda.UpdateAsync(id, request);
                return Ok(action);
            }
            catch (ArgumentException ex)
            {
                return ArgumentExceptionHandling(ex);
            }
            catch (Exception ex)
            {
                return ExceptionHandling(ex);
            }
        }

    }
}
